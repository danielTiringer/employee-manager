<h1>Employee-Manager</h1>
<a href="/new" class="btn btn-primary">Add new employee</a>
<table>
    <tr>
        <th>Name</th>
    </tr>
    @foreach ($employees as $employee)
        <tr>
            <td><a href="/{{ $employee->id }}">{{ $employee->name }}</a></td>
        </tr>
    @endforeach
</table>
