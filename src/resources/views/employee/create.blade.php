<form action="/new" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name">
        @error('name')
            <p>{{ $message }}</p>
        @enderror
    </div>
    <div class="form-group">
        <label for="data">Data</label>
        <input type="text" class="form-control" id="data" name="data">
        @error('data')
            <p>{{ $message }}</p>
        @enderror
    </div>
    <div class="form-group">
        <label for="manager">Manager</label>
        <input type="text" class="form-control" id="manager" name="manager">
        @error('manager')
            <p>{{ $message }}</p>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
</form>
