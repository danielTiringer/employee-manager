<div>
    <h5>Name</h5>
    <p>{{ $name }}</p>
    <h5>Data</h5>
    @foreach ($data as $line)
        <p>{{ $line->metadata }}
    @endforeach
    <p></p>
    <h5>Manager</h5>
    <p>{{ $manager->name }}</p>
</div>
<a href="/" class="btn btn-primary">Back</a>
