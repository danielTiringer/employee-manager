<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeMetadata extends Model
{
    protected $guarded = [];

    public function employee()
    {
        $this->belongsTo('App\Employee');
    }
}
