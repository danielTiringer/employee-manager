<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];

    public function reporteeRelations()
    {
        return $this->hasOne('App\EmployeeRelations', 'reportee_id');
    }

    public function managerRelations()
    {
        return $this->hasMany('App\EmployeeRelations', 'manager_id');
    }

    public function data()
    {
        return $this->hasMany('App\EmployeeMetadata');
    }
}
