<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeMetadata;
use App\EmployeeRelations;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::all();
        return view('employee.index', [
            'employees' => $employees,
        ]);
    }

    public function create()
    {
        return view('employee.create');
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
            'data' => '',
            'manager' => 'nullable|exists:employees,name',
        ]);

        $employee = Employee::create([
            'name' => $data['name'],
        ]);

        if ($data['data']) {
            $metadata = EmployeeMetadata::create([
                'employee_id' => $employee->id,
                'metadata' => $data['data'],
            ]);
        }

        if ($data['manager']) {
            $managerId = Employee::select('id')->where('name', $data['manager']);
            $relations = EmployeeRelations::create([
                'reportee_id' => $employee->id,
                'manager_id' => $managerId,
            ]);
        }

        return redirect('/' . $employee->id);
    }

    public function show(Employee $employee)
    {
        $data = Employee::find($employee->id)->data;
        $managerRelation = Employee::find($employee->id)->reporteeRelations;
        $manager = Employee::find($managerRelation->manager_id);
        return view('employee.show', [
            'name' => $employee->name,
            'data' => $data,
            'manager' => $manager,
        ]);
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }
}
