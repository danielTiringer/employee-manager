<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRelations extends Model
{
    protected $guarded = [];

    public function manager()
    {
        $this->belongsTo('App\Employee', 'id', 'manager_id');
    }

    public function reportee()
    {
        $this->belongsTo('App\Employee', 'id', 'reportee_id');
    }
}
